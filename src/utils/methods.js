const generateVerticalSequences = (dna) =>
  dna.map((value, index, array) =>
    array.map((strand) => strand.split('')[index]).join('')
  );

const findMutations = (dna) => {
  let mutations = 0;
  dna.forEach((value) => {
    let max = 0;
    let count = 1;
    value.split('').forEach((letter, index, array) => {
      if (letter === array[index - 1]) {
        count++;
      } else {
        count = 1;
      }
      max = max > count ? max : count;
    });
    if (max > 3) {
      mutations++;
    }
  });
  return mutations;
};

export const hasMutation = (dna) => {
  dna = [...dna, ...generateVerticalSequences(dna)];
  return findMutations(dna) > 1;
};
