import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import personRoutes from './routes/person.routes';

const app = express();

app.use(cors());
app.use(helmet());
app.use(express.json());

app.use(personRoutes);

app.get('/', (req, res) => {
  res.status(200).send('service is running');
});

export default app;
