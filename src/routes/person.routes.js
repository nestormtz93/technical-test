import { Router } from 'express';
import { mutation, stats } from '../controllers/person.controllers';
import { verifyStrandsDna } from '../middlewares/verify';

const router = Router();

router.post('/mutation', verifyStrandsDna, mutation);

router.get('/stats', stats);

export default router;
