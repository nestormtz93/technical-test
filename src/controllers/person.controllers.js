import Person from '../models/Person';
import { hasMutation } from '../utils/methods';

export const mutation = async (req, res) => {
  try {
    const { dna } = req.body;

    const mutation = hasMutation(dna);

    const exist = await Person.findOne({ dna });

    if (!exist) {
      const person = new Person({ dna, mutation });
      await person.save();
    }

    const status = mutation ? 200 : 403;

    return res.status(status).json(mutation);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const stats = async (req, res) => {
  try {
    const [response] = await Person.aggregate([
      {
        $group: {
          _id: null,
          count_mutations: {
            $sum: { $cond: [{ $eq: ['$mutation', true] }, 1, 0] },
          },
          count_no_mutations: {
            $sum: { $cond: [{ $eq: ['$mutation', false] }, 1, 0] },
          },
        },
      },
      {
        $project: {
          _id: 0,
          count_mutations: 1,
          count_no_mutations: 1,
          ratio: {
            $trunc: [
              { $divide: ['$count_mutations', '$count_no_mutations'] },
              2,
            ],
          },
        },
      },
    ]);

    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json(error);
  }
};
