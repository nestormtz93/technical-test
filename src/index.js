import app from './app';
import { PORT } from './config';
import db from './db';

const main = async () => {
  await db();
  app.listen(PORT, () => {
    console.log('Server on port ', PORT);
  });
};

main();
