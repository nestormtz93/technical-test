import request from 'supertest';
import app from '../app';
import db from '../db';

describe('Endpoints testing', () => {
  beforeEach(async () => {
    await db();
  });

  test('should show statistics of DNA checks', async () => {
    const response = await request(app).get('/stats');

    expect(response.body).toEqual(
      expect.objectContaining({
        count_mutations: expect.any(Number),
        count_no_mutations: expect.any(Number),
        ratio: expect.any(Number),
      })
    );
    expect(response.statusCode).toBe(200);
  });

  test('Should detect that there is a mutation ', async () => {
    const response = await request(app)
      .post('/mutation')
      .send({
        dna: ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG'],
      });
    expect(response.body).toBe(true);
    expect(response.statusCode).toBe(200);
  });

  test('Should detect that there isn´t mutation ', async () => {
    const response = await request(app)
      .post('/mutation')
      .send({
        dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
      });
    expect(response.body).toBe(false);
    expect(response.statusCode).toBe(403);
  });

  test('Should fail for lack of values', async () => {
    const response = await request(app).post('/mutation').send({ dna: [] });
    expect(response.body).toBe('requires 6-strand DNA');
    expect(response.statusCode).toBe(403);
  });

  test('Should fail for lack of characters', async () => {
    const response = await request(app)
      .post('/mutation')
      .send({
        dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACT'],
      });
    expect(response.body).toBe('strand must be 6 characters');
    expect(response.statusCode).toBe(403);
  });

  test('Should fail for wrong characters', async () => {
    const response = await request(app)
      .post('/mutation')
      .send({
        dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTB'],
      });
    expect(response.body).toBe('characters can only be: A, T, C, G');
    expect(response.statusCode).toBe(403);
  });
});
