import { Schema, model } from 'mongoose';

const PersonSchema = new Schema({
  dna: [String],
  mutation: Boolean,
});

export default model('Person', PersonSchema);
