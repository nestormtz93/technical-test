## Link

```bash
http://technical-test-1667694043.us-east-1.elb.amazonaws.com
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev
```

## Test

```bash
# unit tests
$ npm run test
```
