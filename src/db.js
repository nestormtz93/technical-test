import mongoose from 'mongoose';
import { MONGODB_URI } from './config';

export default () =>
  mongoose
    .connect(MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => console.log(`DB is connected`))
    .catch((err) => console.log(err));
