export const verifyStrandsDna = (req, res, next) => {
  const { dna } = req.body;

  if (dna.length !== 6) {
    return res.status(403).json('requires 6-strand DNA');
  }

  let wrongLetters = false;
  let wrongStrand = false;

  const letters = {
    A: true,
    T: true,
    C: true,
    G: true,
  };

  dna.forEach((value) => {
    const strand = value.split('');
    if (strand.length !== 6) {
      wrongStrand = true;
    }
    strand.forEach((letter) => {
      if (!letters[letter]) {
        wrongLetters = true;
      }
    });
  });

  if (wrongStrand) {
    return res.status(403).json('strand must be 6 characters');
  }

  if (wrongLetters) {
    return res.status(403).json('characters can only be: A, T, C, G');
  }

  next();
};
